import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";

@Injectable()
export class AppService {
  constructor(private http: HttpClient) {}
  save(produto: any): Observable<any> {
    return this.http.post(`http://localhost:8080/produto`, produto);
  }
}
