import { Component } from "@angular/core";
import { AppService } from "./app.service";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  title = "my-app-angular";
  nome: string;
  valor: number;

  produto = { nome: "", valor: 0.0 };
  constructor(private appService: AppService) {}

  salvar() {
    console.log(this.produto);
    this.appService.save(this.produto).subscribe();
    this.produto = { nome: "", valor: 0.0 };
  }
}
